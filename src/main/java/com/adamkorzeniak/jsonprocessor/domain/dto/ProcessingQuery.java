package com.adamkorzeniak.jsonprocessor.domain.dto;

import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class ProcessingQuery {

    private Select select = new Select();
    private Filter filter = new Filter();

    public void addSelectIncluded(List<String> tokens) {
        this.select.included.addAll(tokens);
    }
    public void addSelectExcluded(List<String> tokens) {
        this.select.excluded.addAll(tokens);
    }

    @Data
    public static class Select {
        private Set<String> excluded = new HashSet<>();
        private Set<String> included = new HashSet<>();
    }

    @Data
    private static class Filter {
    }
}
