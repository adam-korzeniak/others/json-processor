package com.adamkorzeniak.jsonprocessor.domain;

import com.adamkorzeniak.jsonprocessor.domain.dto.ProcessingQuery;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class JsonProcessingFacade {
    public Map<String, Object> process(Map<String, Object> content, ProcessingQuery query) {
        content = exclude(content, query.getSelect().getExcluded());
        content = include(content, query.getSelect().getIncluded());
        return content;
    }

    private Map<String, Object> exclude(Map<String, Object> content, Set<String> tokens) {
        for (String token : tokens) {
            content = exclude(content, token);
        }
        return content;
    }

    private Map<String, Object> exclude(Map<String, Object> content, String token) {
        String[] tokenPath = token.split("\\.");
        exclude(content, tokenPath);
        return content;
    }

    private void exclude(Map<String, Object> content, String[] tokenPath) {
        assert tokenPath.length != 0;
        if (tokenPath.length == 1) {
            content.remove(tokenPath[0]);
            return;
        }
        String path = tokenPath[0];
        Object pathValue = content.get(path);
        if (pathValue == null) {
            return;
        }
        tokenPath = Arrays.copyOfRange(tokenPath, 1, tokenPath.length);
        if (pathValue instanceof Map) {
            exclude((Map<String, Object>) pathValue, tokenPath);
        } else if (pathValue instanceof Collection) {
            for (Map<String, Object> item : (Collection<Map<String, Object>>) pathValue) {
                exclude(item, tokenPath);
            }
        } else {
            throw new RuntimeException("Type not supported");
        }
    }

    private Map<String, Object> include(Map<String, Object> content, Set<String> tokens) {
        if (tokens.isEmpty()) {
            return content;
        }
        Map<String, Object> result = new HashMap<>();
        for (String token : tokens) {
            result = include(result, content, token);
        }
        return result;
    }

    private Map<String, Object> include(Map<String, Object> result, Map<String, Object> content, String token) {
        String[] tokenPath = token.split("\\.");
        Map<String, Object> temp = (Map<String, Object>) include(content, tokenPath);
        return Stream.concat(result.entrySet().stream(), temp.entrySet().stream()).collect(
                Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Object include(Map<String, Object> content, String[] tokenPath) {
        assert tokenPath.length != 0;
        String path = tokenPath[0];
        Object pathValue = content.get(path);

        if (tokenPath.length == 1) {
            return Map.of(path, pathValue);
        }

        tokenPath = Arrays.copyOfRange(tokenPath, 1, tokenPath.length);
        if (pathValue instanceof Map) {
            Object temp = include((Map<String, Object>) pathValue, tokenPath);
            return Map.of(path, temp);
        } else if (pathValue instanceof Collection) {
            List<Object> tempList = new ArrayList<>();
            for (Map<String, Object> item : (Collection<Map<String, Object>>) pathValue) {
                Object tempItem = include(item, tokenPath);
                tempList.add(tempItem);
            }
            return Map.of(path, tempList);
        } else {
            throw new RuntimeException("Type not supported");
        }

    }

}
