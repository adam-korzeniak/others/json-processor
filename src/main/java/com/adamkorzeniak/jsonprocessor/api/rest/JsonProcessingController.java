package com.adamkorzeniak.jsonprocessor.api.rest;

import com.adamkorzeniak.jsonprocessor.domain.JsonProcessingFacade;
import com.adamkorzeniak.jsonprocessor.domain.dto.ProcessingQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/json/process")
public class JsonProcessingController {

    private final JsonProcessingFacade jsonProcessingFacade;

    @PostMapping
    public Map<String, Object> processJson(@RequestBody Map<String, Object> content, @RequestParam Map<String, String> params) {
        ProcessingQuery query = extract(params);
        return jsonProcessingFacade.process(content, query);
    }
    @PostMapping("list")
    public List<Map<String, Object>> processJson(@RequestBody List<Map<String, Object>> content, @RequestParam Map<String, String> params) {
        ProcessingQuery query = extract(params);
        return content.stream()
                .map(item -> jsonProcessingFacade.process(item, query))
                .collect(Collectors.toList());
    }

    private ProcessingQuery extract(Map<String, String> params) {
        String[] selectTokens = params.getOrDefault("select", "").split(",");
        Map<Boolean, List<String>> partitionedSelectTokens = Arrays.stream(selectTokens)
                .map(String::trim)
                .filter(string -> !string.isBlank())
                .collect(Collectors.partitioningBy(token -> token.startsWith("!")));
        var query = new ProcessingQuery();
        query.addSelectIncluded(partitionedSelectTokens.get(false));
        query.addSelectExcluded(
                partitionedSelectTokens.get(true).stream()
                        .map(token -> token.replaceFirst("!", ""))
                        .collect(Collectors.toList())
        );
        return query;
    }

}
